///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 4.0.0-0-g0efcecf)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "widgets/text_ctrl_eval.h"

#include "dialog_create_symbol_array_base.h"

///////////////////////////////////////////////////////////////////////////

DIALOG_CREATE_SYMBOL_ARRAY_BASE::DIALOG_CREATE_SYMBOL_ARRAY_BASE( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : DIALOG_SHIM( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( -1,-1 ), wxDefaultSize );

	wxBoxSizer* bMainSizer;
	bMainSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxHORIZONTAL );

	m_gridTypeNotebook = new wxNotebook( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	m_gridPanel = new wxPanel( m_gridTypeNotebook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizerGridArray;
	bSizerGridArray = new wxBoxSizer( wxHORIZONTAL );

	wxBoxSizer* bSizerGridLeft;
	bSizerGridLeft = new wxBoxSizer( wxVERTICAL );

	wxStaticBoxSizer* sbSizerGridSize;
	sbSizerGridSize = new wxStaticBoxSizer( new wxStaticBox( m_gridPanel, wxID_ANY, _("Grid Array Size") ), wxVERTICAL );

	wxFlexGridSizer* fgSizerGridSize;
	fgSizerGridSize = new wxFlexGridSizer( 0, 2, 5, 5 );
	fgSizerGridSize->AddGrowableCol( 1 );
	fgSizerGridSize->SetFlexibleDirection( wxBOTH );
	fgSizerGridSize->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_labelNx = new wxStaticText( sbSizerGridSize->GetStaticBox(), wxID_ANY, _("Horizontal count:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_labelNx->Wrap( -1 );
	m_labelNx->SetToolTip( _("Number of columns") );

	fgSizerGridSize->Add( m_labelNx, 0, wxALIGN_CENTER_VERTICAL, 5 );

	m_entryNx = new TEXT_CTRL_EVAL( sbSizerGridSize->GetStaticBox(), wxID_ANY, _("5"), wxDefaultPosition, wxDefaultSize, 0 );
	m_entryNx->SetMinSize( wxSize( 60,-1 ) );

	fgSizerGridSize->Add( m_entryNx, 0, wxEXPAND, 5 );

	m_labelNy = new wxStaticText( sbSizerGridSize->GetStaticBox(), wxID_ANY, _("Vertical count:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_labelNy->Wrap( -1 );
	m_labelNy->SetToolTip( _("Number of rows") );

	fgSizerGridSize->Add( m_labelNy, 0, wxALIGN_CENTER_VERTICAL, 5 );

	m_entryNy = new TEXT_CTRL_EVAL( sbSizerGridSize->GetStaticBox(), wxID_ANY, _("5"), wxDefaultPosition, wxDefaultSize, 0 );
	m_entryNy->SetToolTip( _("Number of rows") );
	m_entryNy->SetMinSize( wxSize( 60,-1 ) );

	fgSizerGridSize->Add( m_entryNy, 0, wxEXPAND, 5 );


	sbSizerGridSize->Add( fgSizerGridSize, 1, wxEXPAND|wxBOTTOM|wxRIGHT|wxLEFT, 5 );


	bSizerGridLeft->Add( sbSizerGridSize, 0, wxBOTTOM|wxEXPAND|wxLEFT|wxRIGHT, 10 );

	wxStaticBoxSizer* sbSizerItemsSpacing;
	sbSizerItemsSpacing = new wxStaticBoxSizer( new wxStaticBox( m_gridPanel, wxID_ANY, _("Items Spacing") ), wxVERTICAL );

	wxFlexGridSizer* fgSizerItemSpacing;
	fgSizerItemSpacing = new wxFlexGridSizer( 0, 3, 5, 5 );
	fgSizerItemSpacing->AddGrowableCol( 1 );
	fgSizerItemSpacing->SetFlexibleDirection( wxBOTH );
	fgSizerItemSpacing->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_labelDx = new wxStaticText( sbSizerItemsSpacing->GetStaticBox(), wxID_ANY, _("Horizontal spacing:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_labelDx->Wrap( -1 );
	m_labelDx->SetToolTip( _("Distance between columns") );

	fgSizerItemSpacing->Add( m_labelDx, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );

	m_entryDx = new wxTextCtrl( sbSizerItemsSpacing->GetStaticBox(), wxID_ANY, _("5"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizerItemSpacing->Add( m_entryDx, 0, wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_unitLabelDx = new wxStaticText( sbSizerItemsSpacing->GetStaticBox(), wxID_ANY, _("mm"), wxDefaultPosition, wxDefaultSize, 0 );
	m_unitLabelDx->Wrap( -1 );
	fgSizerItemSpacing->Add( m_unitLabelDx, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );

	m_labelDy = new wxStaticText( sbSizerItemsSpacing->GetStaticBox(), wxID_ANY, _("Vertical spacing:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_labelDy->Wrap( -1 );
	m_labelDy->SetToolTip( _("Distance between rows") );

	fgSizerItemSpacing->Add( m_labelDy, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );

	m_entryDy = new wxTextCtrl( sbSizerItemsSpacing->GetStaticBox(), wxID_ANY, _("5"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizerItemSpacing->Add( m_entryDy, 0, wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_unitLabelDy = new wxStaticText( sbSizerItemsSpacing->GetStaticBox(), wxID_ANY, _("mm"), wxDefaultPosition, wxDefaultSize, 0 );
	m_unitLabelDy->Wrap( -1 );
	fgSizerItemSpacing->Add( m_unitLabelDy, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );


	fgSizerItemSpacing->Add( 0, 5, 1, wxEXPAND, 5 );


	fgSizerItemSpacing->Add( 0, 0, 1, wxEXPAND, 5 );


	fgSizerItemSpacing->Add( 0, 0, 1, wxEXPAND, 5 );

	m_labelOffsetX = new wxStaticText( sbSizerItemsSpacing->GetStaticBox(), wxID_ANY, _("Horizontal offset:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_labelOffsetX->Wrap( -1 );
	m_labelOffsetX->SetToolTip( _("Offset added to the next row position.") );

	fgSizerItemSpacing->Add( m_labelOffsetX, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );

	m_entryOffsetX = new wxTextCtrl( sbSizerItemsSpacing->GetStaticBox(), wxID_ANY, _("0"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizerItemSpacing->Add( m_entryOffsetX, 0, wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_unitLabelOffsetX = new wxStaticText( sbSizerItemsSpacing->GetStaticBox(), wxID_ANY, _("mm"), wxDefaultPosition, wxDefaultSize, 0 );
	m_unitLabelOffsetX->Wrap( -1 );
	fgSizerItemSpacing->Add( m_unitLabelOffsetX, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );

	m_labelOffsetY = new wxStaticText( sbSizerItemsSpacing->GetStaticBox(), wxID_ANY, _("Vertical offset:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_labelOffsetY->Wrap( -1 );
	m_labelOffsetY->SetToolTip( _("Offset added to the next column position") );

	fgSizerItemSpacing->Add( m_labelOffsetY, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );

	m_entryOffsetY = new wxTextCtrl( sbSizerItemsSpacing->GetStaticBox(), wxID_ANY, _("0"), wxDefaultPosition, wxDefaultSize, 0 );
	fgSizerItemSpacing->Add( m_entryOffsetY, 0, wxALIGN_CENTER_VERTICAL|wxEXPAND, 5 );

	m_unitLabelOffsetY = new wxStaticText( sbSizerItemsSpacing->GetStaticBox(), wxID_ANY, _("mm"), wxDefaultPosition, wxDefaultSize, 0 );
	m_unitLabelOffsetY->Wrap( -1 );
	fgSizerItemSpacing->Add( m_unitLabelOffsetY, 0, wxALIGN_CENTER_VERTICAL|wxRIGHT, 5 );


	sbSizerItemsSpacing->Add( fgSizerItemSpacing, 1, wxEXPAND|wxBOTTOM, 5 );


	bSizerGridLeft->Add( sbSizerItemsSpacing, 0, wxEXPAND|wxBOTTOM|wxRIGHT|wxLEFT, 10 );

	wxStaticBoxSizer* sbSizerStagger;
	sbSizerStagger = new wxStaticBoxSizer( new wxStaticBox( m_gridPanel, wxID_ANY, _("Stagger Settings") ), wxVERTICAL );

	wxBoxSizer* bSizerStaggerset;
	bSizerStaggerset = new wxBoxSizer( wxHORIZONTAL );

	m_labelStagger = new wxStaticText( sbSizerStagger->GetStaticBox(), wxID_ANY, _("Stagger:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_labelStagger->Wrap( -1 );
	m_labelStagger->SetToolTip( _("Value -1,  0 or 1 disable this option.") );

	bSizerStaggerset->Add( m_labelStagger, 0, wxALIGN_CENTER_VERTICAL|wxLEFT, 5 );

	m_entryStagger = new TEXT_CTRL_EVAL( sbSizerStagger->GetStaticBox(), wxID_ANY, _("1"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizerStaggerset->Add( m_entryStagger, 1, wxRIGHT|wxLEFT|wxEXPAND, 5 );


	sbSizerStagger->Add( bSizerStaggerset, 1, wxEXPAND, 5 );

	m_staggerRows = new wxRadioButton( sbSizerStagger->GetStaticBox(), wxID_ANY, _("Rows"), wxDefaultPosition, wxDefaultSize, wxRB_GROUP );
	sbSizerStagger->Add( m_staggerRows, 0, wxALL, 5 );

	m_staggerCols = new wxRadioButton( sbSizerStagger->GetStaticBox(), wxID_ANY, _("Columns"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staggerCols->SetValue( true );
	sbSizerStagger->Add( m_staggerCols, 0, wxBOTTOM|wxRIGHT|wxLEFT, 5 );


	bSizerGridLeft->Add( sbSizerStagger, 0, wxEXPAND|wxBOTTOM|wxRIGHT|wxLEFT, 10 );


	bSizerGridArray->Add( bSizerGridLeft, 1, wxEXPAND, 5 );

	m_gridPinNumberingPanel = new wxPanel( m_gridPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_gridPinNumberingPanel->Hide();

	wxBoxSizer* bSizer15;
	bSizer15 = new wxBoxSizer( wxVERTICAL );

	m_gridPinNumberingSizer = new wxBoxSizer( wxVERTICAL );

	wxString m_radioBoxGridNumberingAxisChoices[] = { _("Horizontal, then vertical"), _("Vertical, then horizontal") };
	int m_radioBoxGridNumberingAxisNChoices = sizeof( m_radioBoxGridNumberingAxisChoices ) / sizeof( wxString );
	m_radioBoxGridNumberingAxis = new wxRadioBox( m_gridPinNumberingPanel, wxID_ANY, _("Numbering Direction"), wxDefaultPosition, wxDefaultSize, m_radioBoxGridNumberingAxisNChoices, m_radioBoxGridNumberingAxisChoices, 1, wxRA_SPECIFY_COLS );
	m_radioBoxGridNumberingAxis->SetSelection( 0 );
	m_gridPinNumberingSizer->Add( m_radioBoxGridNumberingAxis, 0, wxEXPAND|wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	m_checkBoxGridReverseNumbering = new wxCheckBox( m_gridPinNumberingPanel, wxID_ANY, _("Reverse numbering on alternate rows/columns"), wxDefaultPosition, wxDefaultSize, 0 );
	m_gridPinNumberingSizer->Add( m_checkBoxGridReverseNumbering, 0, wxALL, 5 );

	wxString m_rbGridStartNumberingOptChoices[] = { _("Use first free number"), _("From start value") };
	int m_rbGridStartNumberingOptNChoices = sizeof( m_rbGridStartNumberingOptChoices ) / sizeof( wxString );
	m_rbGridStartNumberingOpt = new wxRadioBox( m_gridPinNumberingPanel, wxID_ANY, _("Initial Pin Number"), wxDefaultPosition, wxDefaultSize, m_rbGridStartNumberingOptNChoices, m_rbGridStartNumberingOptChoices, 1, wxRA_SPECIFY_COLS );
	m_rbGridStartNumberingOpt->SetSelection( 1 );
	m_gridPinNumberingSizer->Add( m_rbGridStartNumberingOpt, 0, wxALL|wxEXPAND, 5 );

	wxString m_radioBoxGridNumberingSchemeChoices[] = { _("Continuous (1, 2, 3...)"), _("Coordinate (A1, A2, ... B1, ...)") };
	int m_radioBoxGridNumberingSchemeNChoices = sizeof( m_radioBoxGridNumberingSchemeChoices ) / sizeof( wxString );
	m_radioBoxGridNumberingScheme = new wxRadioBox( m_gridPinNumberingPanel, wxID_ANY, _("Pin Numbering Scheme"), wxDefaultPosition, wxDefaultSize, m_radioBoxGridNumberingSchemeNChoices, m_radioBoxGridNumberingSchemeChoices, 1, wxRA_SPECIFY_COLS );
	m_radioBoxGridNumberingScheme->SetSelection( 1 );
	m_gridPinNumberingSizer->Add( m_radioBoxGridNumberingScheme, 0, wxALL|wxEXPAND, 5 );

	m_labelPriAxisNumbering = new wxStaticText( m_gridPinNumberingPanel, wxID_ANY, _("Primary axis numbering:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_labelPriAxisNumbering->Wrap( -1 );
	m_gridPinNumberingSizer->Add( m_labelPriAxisNumbering, 0, wxLEFT|wxRIGHT|wxTOP, 5 );

	wxArrayString m_choicePriAxisNumberingChoices;
	m_choicePriAxisNumbering = new wxChoice( m_gridPinNumberingPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choicePriAxisNumberingChoices, 0 );
	m_choicePriAxisNumbering->SetSelection( 0 );
	m_gridPinNumberingSizer->Add( m_choicePriAxisNumbering, 0, wxEXPAND|wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	m_labelSecAxisNumbering = new wxStaticText( m_gridPinNumberingPanel, wxID_ANY, _("Secondary axis numbering:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_labelSecAxisNumbering->Wrap( -1 );
	m_labelSecAxisNumbering->Enable( false );

	m_gridPinNumberingSizer->Add( m_labelSecAxisNumbering, 0, wxTOP|wxRIGHT|wxLEFT, 5 );

	wxArrayString m_choiceSecAxisNumberingChoices;
	m_choiceSecAxisNumbering = new wxChoice( m_gridPinNumberingPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, m_choiceSecAxisNumberingChoices, 0 );
	m_choiceSecAxisNumbering->SetSelection( 0 );
	m_choiceSecAxisNumbering->Enable( false );

	m_gridPinNumberingSizer->Add( m_choiceSecAxisNumbering, 0, wxEXPAND|wxBOTTOM|wxRIGHT|wxLEFT, 5 );

	wxFlexGridSizer* fgSizer1;
	fgSizer1 = new wxFlexGridSizer( 2, 3, 0, 0 );
	fgSizer1->AddGrowableCol( 0 );
	fgSizer1->SetFlexibleDirection( wxBOTH );
	fgSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_labelGridNumberingOffset = new wxStaticText( m_gridPinNumberingPanel, wxID_ANY, _("Pin numbering start:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_labelGridNumberingOffset->Wrap( -1 );
	fgSizer1->Add( m_labelGridNumberingOffset, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_entryGridPriNumberingOffset = new wxTextCtrl( m_gridPinNumberingPanel, wxID_ANY, _("1"), wxDefaultPosition, wxDefaultSize, 0 );
	m_entryGridPriNumberingOffset->SetMinSize( wxSize( 72,-1 ) );

	fgSizer1->Add( m_entryGridPriNumberingOffset, 0, wxALL, 5 );

	m_entryGridSecNumberingOffset = new wxTextCtrl( m_gridPinNumberingPanel, wxID_ANY, _("1"), wxDefaultPosition, wxDefaultSize, 0 );
	m_entryGridSecNumberingOffset->SetMinSize( wxSize( 72,-1 ) );

	fgSizer1->Add( m_entryGridSecNumberingOffset, 0, wxALL, 5 );

	m_labelGridNumberingStep = new wxStaticText( m_gridPinNumberingPanel, wxID_ANY, _("Pin numbering increment:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_labelGridNumberingStep->Wrap( -1 );
	fgSizer1->Add( m_labelGridNumberingStep, 1, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

	m_entryGridPriNumberingStep = new wxTextCtrl( m_gridPinNumberingPanel, wxID_ANY, _("1"), wxDefaultPosition, wxDefaultSize, 0 );
	m_entryGridPriNumberingStep->SetMinSize( wxSize( 72,-1 ) );

	fgSizer1->Add( m_entryGridPriNumberingStep, 0, wxALL, 5 );

	m_entryGridSecNumberingStep = new wxTextCtrl( m_gridPinNumberingPanel, wxID_ANY, _("1"), wxDefaultPosition, wxDefaultSize, 0 );
	m_entryGridSecNumberingStep->SetMinSize( wxSize( 72,-1 ) );

	fgSizer1->Add( m_entryGridSecNumberingStep, 0, wxALL, 5 );


	m_gridPinNumberingSizer->Add( fgSizer1, 0, wxEXPAND, 5 );


	bSizer15->Add( m_gridPinNumberingSizer, 1, wxEXPAND|wxLEFT, 10 );


	m_gridPinNumberingPanel->SetSizer( bSizer15 );
	m_gridPinNumberingPanel->Layout();
	bSizer15->Fit( m_gridPinNumberingPanel );
	bSizerGridArray->Add( m_gridPinNumberingPanel, 1, wxEXPAND, 5 );


	m_gridPanel->SetSizer( bSizerGridArray );
	m_gridPanel->Layout();
	bSizerGridArray->Fit( m_gridPanel );
	m_gridTypeNotebook->AddPage( m_gridPanel, _("Grid Array"), false );

	bSizer7->Add( m_gridTypeNotebook, 1, wxALL|wxEXPAND, 10 );

	m_symbolReannotatePanel = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer( wxVERTICAL );

	wxStaticBoxSizer* sbSizerSymbolAnnotation;
	sbSizerSymbolAnnotation = new wxStaticBoxSizer( new wxStaticBox( m_symbolReannotatePanel, wxID_ANY, _("Symbol Annotation") ), wxVERTICAL );

	m_radioBtnKeepRefs = new wxRadioButton( sbSizerSymbolAnnotation->GetStaticBox(), wxID_ANY, _("Keep existing reference designators"), wxDefaultPosition, wxDefaultSize, 0 );
	sbSizerSymbolAnnotation->Add( m_radioBtnKeepRefs, 0, wxALL, 5 );

	m_radioBtnUniqueRefs = new wxRadioButton( sbSizerSymbolAnnotation->GetStaticBox(), wxID_ANY, _("Assign unique reference designators"), wxDefaultPosition, wxDefaultSize, 0 );
	m_radioBtnUniqueRefs->SetValue( true );
	m_radioBtnUniqueRefs->SetToolTip( _("This can conflict with reference designators in the schematic that have not yet been synchronized with the board.") );

	sbSizerSymbolAnnotation->Add( m_radioBtnUniqueRefs, 0, wxBOTTOM|wxLEFT|wxRIGHT, 5 );


	bSizer8->Add( sbSizerSymbolAnnotation, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );


	m_symbolReannotatePanel->SetSizer( bSizer8 );
	m_symbolReannotatePanel->Layout();
	bSizer8->Fit( m_symbolReannotatePanel );
	bSizer7->Add( m_symbolReannotatePanel, 1, wxEXPAND | wxALL, 5 );


	bMainSizer->Add( bSizer7, 1, wxEXPAND, 5 );

	m_stdButtons = new wxStdDialogButtonSizer();
	m_stdButtonsOK = new wxButton( this, wxID_OK );
	m_stdButtons->AddButton( m_stdButtonsOK );
	m_stdButtonsCancel = new wxButton( this, wxID_CANCEL );
	m_stdButtons->AddButton( m_stdButtonsCancel );
	m_stdButtons->Realize();

	bMainSizer->Add( m_stdButtons, 0, wxALL|wxEXPAND, 5 );


	this->SetSizer( bMainSizer );
	this->Layout();
	bMainSizer->Fit( this );

	// Connect Events
	this->Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnClose ) );
	m_entryNx->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_entryNy->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_entryDx->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_entryDy->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_entryOffsetX->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_entryOffsetY->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_entryStagger->Connect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_rbGridStartNumberingOpt->Connect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_radioBoxGridNumberingScheme->Connect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
}

DIALOG_CREATE_SYMBOL_ARRAY_BASE::~DIALOG_CREATE_SYMBOL_ARRAY_BASE()
{
	// Disconnect Events
	this->Disconnect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnClose ) );
	m_entryNx->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_entryNy->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_entryDx->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_entryDy->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_entryOffsetX->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_entryOffsetY->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_entryStagger->Disconnect( wxEVT_COMMAND_TEXT_UPDATED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_rbGridStartNumberingOpt->Disconnect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );
	m_radioBoxGridNumberingScheme->Disconnect( wxEVT_COMMAND_RADIOBOX_SELECTED, wxCommandEventHandler( DIALOG_CREATE_SYMBOL_ARRAY_BASE::OnParameterChanged ), NULL, this );

}
