/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Created on: 11 Mar 2016, author John Beard
 * Copyright (C) 1992-2016 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

#ifndef SYMBOL_ARRAY_CREATOR_H
#define SYMBOL_ARRAY_CREATOR_H

#include <vector>
#include <memory>
#include <sch_edit_frame.h>
#include <tool/tool_manager.h>
#include <tools/ee_selection_tool.h>

class SYMBOL_ARRAY_CREATOR
{
public:
    SYMBOL_ARRAY_CREATOR( SCH_EDIT_FRAME& aParent, bool aIsSymbolEditor, EE_SELECTION& aSelection,
                          TOOL_MANAGER* aToolMgr ) :
            m_parent( aParent ),
            m_isSymbolEditor( aIsSymbolEditor ), m_selection( aSelection ), m_toolMgr( aToolMgr )
    {
    }

    void Invoke( SCH_EDIT_FRAME& m_frame );

    void updatePastedSymbol( SCH_SYMBOL* aSymbol, const SCH_SHEET_PATH& aPastePath,
                             const KIID_PATH& aClipPath, bool aForceKeepAnnotations,
                             SCH_EDIT_FRAME& m_frame );

private:
    SCH_EDIT_FRAME& m_parent;
    bool            m_isSymbolEditor;
    EE_SELECTION&   m_selection;
    TOOL_MANAGER*   m_toolMgr;
};

#endif