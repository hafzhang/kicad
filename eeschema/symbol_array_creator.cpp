/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Created on: 11 Mar 2016, author John Beard
 * Copyright (C) 2016-2024 KiCad Developers, see AUTHORS.txt for contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

#include "symbol_array_creator.h"

#include <array_pin_number_provider.h>

#include <dialogs/dialog_create_symbol_array.h>
#include <tool/tool_manager.h>
#include <ee_actions.h>
#include <kiid.h>
#include <tools/ee_tool_base.h>

#include <sch_commit.h>

#include <wx/debug.h>
#include <wx/log.h>
#include <string_utils.h>

/**
 * Transform a #SCH_ITEM from the given #ARRAY_OPTIONS and an index into the array.
 *
 * @param aArrOpts The array options that describe the array
 * @param aIndex   The index in the array of this item
 * @param aItem    The item to transform
 */
static void TransformItem( const ARRAY_OPTIONS& aArrOpts, int aIndex, SCH_ITEM& aItem )
{
    const ARRAY_OPTIONS::TRANSFORM transform = aArrOpts.GetTransform( aIndex, aItem.GetPosition() );

    aItem.Move( transform.m_offset );
}


/**
 * Flag to enable schematic create array debugging output.
 *
 * @ingroup trace_env_vars
 */
static const wxChar traceSchArray[] = wxT( "KICAD_SCH_ARRAY" );


void SYMBOL_ARRAY_CREATOR::Invoke( SCH_EDIT_FRAME& m_frame )
{
    // bail out if no items
    if( m_selection.Size() == 0 )
        return;

    SCH_SHEET_PATH    sheetPath = m_parent.GetCurrentSheet();
    SCH_SYMBOL*       item = dynamic_cast<SCH_SYMBOL*>( m_selection.Items()[0] );
    SCH_SYMBOL* const schSym = m_isSymbolEditor ? item : nullptr;

    const bool enableArrayNumbering = m_isSymbolEditor; // 假设符号阵列需要编号
    VECTOR2I   origin;

    std::map<SCH_SHEET_PATH, SCH_REFERENCE_LIST> pastedSymbols;
    SCH_SHEET_LIST hierarchy = m_frame.Schematic().BuildSheetListSortedByPageNumbers();
    SCH_SHEET_LIST sheetPathsForScreen = hierarchy.FindAllSheetsForScreen( sheetPath.LastScreen() );


    origin = m_selection.Items()[0]->GetPosition();


    std::unique_ptr<ARRAY_OPTIONS> array_opts;

    DIALOG_CREATE_SYMBOL_ARRAY dialog( &m_parent, array_opts, enableArrayNumbering, origin );

    int ret = dialog.ShowModal();

    if( ret != wxID_OK || array_opts == nullptr )
        return;

    SCH_COMMIT commit( &m_parent );

    ARRAY_PIN_NUMBER_PROVIDER pad_number_provider( schSym, *array_opts );

    EDA_ITEMS all_added_items;


    // Keep track of pasted sheets and symbols for the different paths to the hierarchy.
    for( int ptN = 0; ptN < array_opts->GetArraySize(); ptN++ )
    {
        EE_SELECTION          items_for_this_block;
        std::set<SCH_SYMBOL*> SymDeDupe;

        for( int i = 0; i < m_selection.Size(); ++i )
        {
            if( !m_selection[i]->IsSCH_ITEM() )
                continue;


            SCH_ITEM* item = static_cast<SCH_ITEM*>( m_selection[i] );

            SCH_ITEM* this_item = nullptr;

            if( ptN == 0 )
            {
                // the first point: we don't own this or add it, but
                // we might still modify it (position or label)
                this_item = item;
                TransformItem( *array_opts, ptN, *this_item );
            }
            else
            {
                if( m_isSymbolEditor )
                {
                    if( item->Type() == SCH_SYMBOL_T )
                    {
                        this_item = (SCH_ITEM*) item->Duplicate();
                    }
                }


                // Add new items to selection (footprints in the selection will be reannotated)
                items_for_this_block.Add( this_item );

                if( this_item )
                {
                    // Because aItem is/can be created from a selected item, and inherits from
                    // it this state, reset the selected stated of aItem:
                    this_item->ClearSelected();

                    TransformItem( *array_opts, ptN, *this_item );
                }
            }
        }
        for( EDA_ITEM* item : items_for_this_block )
            all_added_items.push_back( item );
    }


    bool ForceKeepAnnotations = !array_opts->ShouldReannotateFootprints();

    //Process the copied symbol and add it to the screen.
    for( EDA_ITEM* item : all_added_items )
    {
        KIID_PATH clipPath( wxT( "/" ) ); // clipboard is at root

        SCH_ITEM* schItem = static_cast<SCH_ITEM*>( item );

        wxCHECK2( schItem, continue );

        if( schItem->IsConnectable() )
            schItem->SetConnectivityDirty();


        SCH_SYMBOL* symbol = static_cast<SCH_SYMBOL*>( item );

        // The library symbol gets set from the cached library symbols in the current
        // schematic not the symbol libraries.  The cached library symbol may have
        // changed from the original library symbol which would cause the copy to
        // be incorrect.
        SCH_SCREEN* currentScreen = m_frame.GetScreen();

        wxCHECK2( currentScreen, continue );

        auto it = currentScreen->GetLibSymbols().find( symbol->GetSchSymbolLibraryName() );
        auto end = currentScreen->GetLibSymbols().end();


        LIB_SYMBOL* libSymbol = nullptr;

        if( it != end )
        {
            libSymbol = new LIB_SYMBOL( *it->second );
            symbol->SetLibSymbol( libSymbol );
        }

        for( SCH_SHEET_PATH& sheetPath : sheetPathsForScreen )
            updatePastedSymbol( symbol, sheetPath, clipPath, ForceKeepAnnotations, m_frame );

        // Assign a new KIID
        const_cast<KIID&>( item->m_Uuid ) = KIID();

        // Make sure pins get a new UUID
        for( SCH_PIN* pin : symbol->GetPins() )
        {
            const_cast<KIID&>( pin->m_Uuid ) = KIID();
            pin->SetConnectivityDirty();
        }

        for( SCH_SHEET_PATH& sheetPath : sheetPathsForScreen )
        {
            // Ignore symbols from a non-existant library.
            if( libSymbol )
            {
                SCH_REFERENCE schReference( symbol, sheetPath );
                schReference.SetSheetNumber( sheetPath.GetVirtualPageNumber() );
                pastedSymbols[sheetPath].AddItem( schReference );
            }
        }

        if( !m_frame.GetScreen()->CheckIfOnDrawList( (SCH_ITEM*) item ) ) // don't want a loop!
            m_frame.AddToScreen( item, m_frame.GetScreen() );

        commit.Added( (SCH_ITEM*) item, m_frame.GetScreen() );
    }

    // Add the symbol to the annotatedSymbols collection, so that it can be re-annotated later.
    std::map<SCH_SHEET_PATH, SCH_REFERENCE_LIST> annotatedSymbols;

    for( const SCH_SHEET_PATH& sheetPath : sheetPathsForScreen )
    {
        for( size_t i = 0; i < pastedSymbols[sheetPath].GetCount(); i++ )
        {
            if( !ForceKeepAnnotations )
            {
                annotatedSymbols[sheetPath].AddItem( pastedSymbols[sheetPath][i] );
            }
        }
    }

    // Construct a symbol reference list object, to store the list of symbol references.
    SCH_REFERENCE_LIST existingRefs;
    hierarchy.GetSymbols( existingRefs );
    existingRefs.SortByReferenceOnly();

    if( !annotatedSymbols.empty() )
    {
        for( SCH_SHEET_PATH& path : sheetPathsForScreen )
        {
            annotatedSymbols[path].SortByReferenceOnly();


            annotatedSymbols[path].ReannotateDuplicates( existingRefs );

            annotatedSymbols[path].UpdateAnnotation();

            // Update existing refs for next iteration
            for( size_t i = 0; i < annotatedSymbols[path].GetCount(); i++ )
                existingRefs.AddItem( annotatedSymbols[path][i] );
        }
    }


    // Updates all screen references for the current form
    m_frame.GetCurrentSheet().UpdateAllScreenReferences();

    // Pass all_added_items as a parameter to add the newly added item to the selection.
    m_toolMgr->RunAction( EE_ACTIONS::clearSelection );
    m_toolMgr->RunAction<EDA_ITEMS*>( EE_ACTIONS::addItemsToSel, &all_added_items );

    commit.Push( _( "Create Array" ) );
    m_frame.RefreshNetNavigator();
}


void SYMBOL_ARRAY_CREATOR::updatePastedSymbol( SCH_SYMBOL*           aSymbol,
                                               const SCH_SHEET_PATH& aPastePath,
                                               const KIID_PATH&      aClipPath,
                                               bool aForceKeepAnnotations, SCH_EDIT_FRAME& m_frame )
{
    // wxCHECK( m_frame && aSymbol, /* void */ );

    SCH_SYMBOL_INSTANCE newInstance;
    bool                instanceFound = false;
    KIID_PATH           pasteLookupPath = aClipPath;

    for( const SCH_SYMBOL_INSTANCE& tmp : aSymbol->GetInstances() )
    {
        if( ( tmp.m_Path.empty() && aClipPath.empty() )
            || ( !aClipPath.empty() && tmp.m_Path.EndsWith( aClipPath ) ) )
        {
            newInstance = tmp;
            instanceFound = true;

            wxLogTrace( traceSchArray,
                        wxS( "Pasting found symbol instance with reference %s, unit %d:"
                             "\n\tClipboard path: %s\n\tSymbol UUID: %s." ),
                        tmp.m_Reference, tmp.m_Unit, aClipPath.AsString(),
                        aSymbol->m_Uuid.AsString() );

            break;
        }
    }

    // The pasted symbol look up paths include the symbol UUID.
    pasteLookupPath.push_back( aSymbol->m_Uuid );

    if( !instanceFound )
    {
        wxLogTrace( traceSchArray,
                    wxS( "Clipboard symbol instance **not** found:\n\tClipboard path: %s\n\t"
                         "Symbol UUID: %s." ),
                    aClipPath.AsString(), aSymbol->m_Uuid.AsString() );

        // Some legacy versions saved value fields escaped.  While we still do in the symbol
        // editor, we don't anymore in the schematic, so be sure to unescape them.
        SCH_FIELD* valueField = aSymbol->GetField( VALUE_FIELD );
        valueField->SetText( UnescapeString( valueField->GetText() ) );

        // Pasted from notepad or an older instance of eeschema.  Use the values in the fields
        // instead.
        newInstance.m_Reference = aSymbol->GetField( REFERENCE_FIELD )->GetText();
        newInstance.m_Unit = aSymbol->GetUnit();
    }

    newInstance.m_Path = aPastePath.Path();
    newInstance.m_ProjectName = m_frame.Prj().GetProjectName();

    aSymbol->AddHierarchicalReference( newInstance );

    if( !aForceKeepAnnotations )
        aSymbol->ClearAnnotation( &aPastePath, false );

    // We might clear annotations but always leave the original unit number from the paste.
    aSymbol->SetUnit( newInstance.m_Unit );
}
