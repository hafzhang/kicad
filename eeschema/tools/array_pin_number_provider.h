/*
 * This program source code file is part of KiCad, a free EDA CAD application.
 *
 * Copyright (C) 2019 KiCad Developers, see AUTHORS.TXT for contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, you may find one here:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * or you may search the http://www.gnu.org website for the version 2 license,
 * or you may write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
 */

#ifndef ARRAY_PIN_NUMBER_PROVIDER__H
#define ARRAY_PIN_NUMBER_PROVIDER__H


#include <array_options.h>
#include <sch_symbol.h>

/**
 * Simple class that sequentially provides numbers from an #ARRAY_OPTIONS object, making sure
 * that they do not conflict with numbers already existing in a #SYMBOL.
 */
class ARRAY_PIN_NUMBER_PROVIDER
{
public:
    /**
     * @param aSymbol the symbol to gather existing numbers from (nullptr for no symbol)
     * @param aArrayOpts the array options that provide the candidate numbers
     */
    ARRAY_PIN_NUMBER_PROVIDER( const SCH_SYMBOL* aSymbol, const ARRAY_OPTIONS& aArrayOpts );

    /**
     * Get the next available PIN name.
     */
    wxString GetNextPinNumber();

private:
    /**
     * Get the next number from a given index/list combo
     * @param  aIndex    index to start at, will be updated
     * @param  aExisting the set of existing numbers to skip
     * @return           the first number found that's not in aExisting
     */
    wxString getNextNumber( int& aIndex, const std::set<wxString>& aExisting );

    const ARRAY_OPTIONS& m_arrayOpts;
    std::set<wxString>   m_existing_pin_numbers;
    int                  m_current_pin_index;
};

#endif // ARRAY_PIN_NUMBER_PROVIDER__H
